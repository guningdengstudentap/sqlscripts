USE ModernWayGS;
INSERT INTO Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Categorie)
VALUES
  ('Céline', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970','Filosofie'),
  ('Celine' ,'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954','Filosofie'),
  ('CELINE', 'Sarthe', 'Het zijn en het niets', 'Parijs', 'Gallimard', '1943','Filosofie');
SELECT * 
FROM Boeken 
WHERE Voornaam = 'Celine';