CREATE DATABASE  IF NOT EXISTS `ModernWaysGSO`;
USE ModernWaysGSO;
CREATE TABLE `Boeken` (
  `Voornaam` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Familienaam` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Titel` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Stad` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Verschijningsjaar` VARCHAR(4) DEFAULT NULL,
  `Uitgeverij` VARCHAR(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Herdruk` VARCHAR(4) DEFAULT NULL,
  `Commentaar` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Categorie` VARCHAR(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IngevoegdDoor` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Huisdieren` (
  `Naam` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Leeftijd` smallint(5) unsigned NOT NULL,
  `Soort` VARCHAR(50) NOT NULL,
  `Baasje` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Geluid` VARCHAR(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Liedjes` (
  `Titel` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Artiest` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Jaar` CHAR(4) DEFAULT NULL,
  `Album` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Aantalbeluisteringen` INT DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Metingen` (
  `Tijdstip` datetime NOT NULL,
  `Grootte` smallint(5) unsigned NOT NULL,
  `Marge` float(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO boeken VALUES
('Aurelius','Augustinus',NULL,NULL,NULL,NULL,NULL,NULL,'Metafysica',NULL),
('Stephen','Hawking','Antwoorden op de grote vragen',NULL,NULL,NULL,NULL,NULL,'Filosofie',NULL),
('William','Dunham','Journey through Genius: The Great Theorems of Mathematics',NULL,NULL,NULL,NULL,NULL,'Wiskunde',NULL),
('Evert Willem','Beth','Mathematical Thought',NULL,'2010',NULL,NULL,NULL,'Filosofie',NULL),
('Jef','B','Het Boek',NULL,'2015',NULL,NULL,NULL,'Filosofie',NULL),
('Mathijs','Degrote','Leren werken met SQL',NULL,NULL,NULL,NULL,'Kan nooit kwaad','Informatica',NULL),
('Mathijs','Degrote','Leren werken met HTML en CSS',NULL,NULL,NULL,NULL,'Kan nooit kwaad','Informatica',NULL),
('Mathijs','Degrote','Leren werken met Javascript',NULL,NULL,NULL,NULL,'Kan nooit kwaad','Informatica',NULL),
('Tom','Van Wommel','Auteursrecht',NULL,NULL,NULL,NULL,'Nuttig om te weten','Recht',NULL),
('Kris','Van Laer','Schaken',NULL,NULL,NULL,NULL,'Veel goede tips','Entertainment',NULL),
('Ellen','Reynaert','Het criminele brein',NULL,NULL,NULL,NULL,NULL,'Psychologie',NULL),
('Céline','Claus','De verwondering','Antwerpen','1970','Manteau',NULL,NULL,'Filosofie',NULL),
('Celine','Raes','Jagen en gejaagd worden','Antwerpen','1954','De Bezige Bij',NULL,NULL,'Filosofie',NULL),
('CELINE','Sarthe','Het zijn en het niets','Parijs','1943','Gallimard',NULL,NULL,'Filosofie',NULL),
(NULL,'?','Beowulf',NULL,'0975',NULL,NULL,NULL,'Mythologie',NULL),
(NULL,'Ovidius','Metamorfosen',NULL,'8',NULL,NULL,NULL,'Mythologie',NULL);

INSERT INTO huisdieren VALUES 
('Misty',6,'hond','Vincent','WAF!'),
('Ming',9,'hond','Christiane','WAF!'),
('Bientje',6,'kat','Esther','miauwww...'),
('Ming',9,'kat','Bert','miauwww...'),
('Фёдор',1,'hond','Lyssa','WAF!');

INSERT INTO liedjes VALUES 
('Stairway to Heaven','Led Zeppelin','1971','Led Zeppelin IV',60),
('Rock and Roll','Led Zeppelin','1971','Led Zeppelin IV',49),
('Riders on the Storm','The Doors','1971','L.A. Woman',30),
('Good Enough','Molly Tuttle','2017','Rise',25),
('Outrage for the Execution of Willie McGee','Goodnight, Texas','2018','Conductor',20),
('They Lie','Layla Zoe','2013','The Lily',40),
('Green Eyed Lover','Layla Zoe','2013','The Lily',40),
('Why You So Afraid','Layla Zoe','2013','The Lily',38),
('It Ain\'t You','Danielle Nicole','2015','Wolf Den',50),
('Unchained','Van Halen','1981','Fair Warning',17);




