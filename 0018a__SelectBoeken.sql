use modernwaysdmlmedium;
INSERT INTO Boeken (
Voornaam,
Familienaam,
Titel,
Stad,
Verschijningsjaar,
Uitgeverij,
Herdruk,
Commentaar,
Categorie,
IngevoegdDoor
) VALUES 
('Aurelius','Augustinus',NULL,NULL,NULL,NULL,NULL,NULL,'Metafysica',NULL),
('Diderik','Batens','Logicaboek','','1999','','','','Metafysica',''),
('Stephen','Hawking','The Nature of Space and Time',NULL,'1996',NULL,NULL,NULL,'Wiskunde',NULL),
('Stephen','Hawking','Antwoorden op de grote vragen',NULL,NULL,NULL,NULL,NULL,'Filosofie',NULL),
('William','Dunham','Journey through Genius: The Great Theorems of Mathematics',NULL,NULL,NULL,NULL,NULL,'Wiskunde',NULL),
('William','Dunham','Euler: The Master of Us All',NULL,'1999',NULL,NULL,'Te lezen','Geschiedenis',NULL),
('Evert Willem','Beth','Mathematical Thought',NULL,'2010',NULL,NULL,NULL,'Filosofie',NULL),
('Jef','B','Het Boek',NULL,'2015',NULL,NULL,NULL,'Filosofie',NULL),
('Mathijs','Degrote','Leren werken met SQL',NULL,NULL,NULL,NULL,'Kan nooit kwaad','Informatica',NULL),
('Tom','Van Wommel','Auteursrecht',NULL,NULL,NULL,NULL,'Nuttig om te weten','Recht',NULL),
('Kris','Van Laer','Schaken',NULL,NULL,NULL,NULL,'Veel goede tips','Entertainment',NULL),
('Ellen','Reynaert','Het criminele brein',NULL,NULL,NULL,NULL,NULL,'Psychologie',NULL);
