USE ModernWaygsDDLM;
INSERT INTO Platformen (Naam)
VALUES
('PS4'),
('Xbox One'),
('Windows'),
('Nintendo Switch');

INSERT INTO Games (Titel)
VALUES
('Anthem'),
('Sekiro: Shadows Die Twice'),
('Devil May Cry 5'),
('Mega Man 11');

INSERT INTO Releases (Games_Id, Platformen_Id)
VALUES
(1,1),
(1,2),
(1,3),
(2,1),
(2,2),
(2,3),
(3,1),
(3,2),
(4,1),
(4,2),
(4,3),
(4,4);