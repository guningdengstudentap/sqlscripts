USE ModernWays;
SELECT Voornaam, Familienaam, Loon
FROM Directieleden
WHERE Loon > ALL (SELECT MAX(Loon) FROM Personeelsleden)
ORDER BY Loon;