USE ModernwaysGSO;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Totaal aantal beluisteringen'
FROM Liedjes
WHERE Length(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;