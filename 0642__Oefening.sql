USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(IN EndingDate DATE, OUT NumberCleaned INT)
BEGIN
START TRANSACTION;
SELECT COUNT(*)
INTO NumberCleaned
FROM Lidmaatschappen
WHERE Lidmaatschappen.Einddatum IS NOT NULL AND Lidmaatschappen.Einddatum < EndingDate;
SET SQL_SAFE_UPDATES = 0;
DELETE
FROM Lidmaatschappen
WHERE Lidmaatschappen.Einddatum IS NOT NULL AND Lidmaatschappen.Einddatum < EndingDate;
SET SQL_SAFE_UPDATES = 1;
COMMIT;
END$$
DELIMITER ;

-- CALL CleanupOldMemberships('1980_01_01', @numberRemoved);
-- SELECT @numberRemoved;