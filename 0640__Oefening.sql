USE Aptunes;
DROP procedure IF EXISTS GetLiedjes;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE GetLiedjes(IN nameList VARCHAR(50))
BEGIN
SELECT Titel
FROM Liedjes
WHERE Titel LIKE CONCAT('%', nameList, '%');

END$$
DELIMITER ;

-- USE Aptunes;
-- CALL GetLiedjes('web');