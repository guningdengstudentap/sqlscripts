USE ModernWaysjoins;
SELECT Leden.Voornaam, Taken.Omschrijving, COALESCE(Taken.Omschrijving)
FROM Leden
LEFT JOIN Taken ON Taken.Leden_Id = Leden.Id
WHERE Leden_Id IS NULL;