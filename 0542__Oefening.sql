USE ModernwaysGSO;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Aantal beluisteringen'
FROM Liedjes 
GROUP BY Artiest
Having SUM(Aantalbeluisteringen) > 100;