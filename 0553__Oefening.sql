USE ModernWaysJOINs;
SET SQL_SAFE_UPDATES = 0;
UPDATE Baasjes
CROSS JOIN Huisdieren
SET Baasjes.Huisdieren_Id = Huisdieren.Id
WHERE Baasjes.Naam = Huisdieren.Baasje;
SET SQL_SAFE_UPDATES = 1;