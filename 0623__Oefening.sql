USE Aptunes;
SELECT Titel, Naam
FROM Liedjesgenres INNER JOIN Liedjes
ON Liedjesgenres.Liedjes_Id = Liedjes.Id
INNER JOIN Genres
ON Liedjesgenres.Genres_Id = Genres.Id
WHERE Naam = 'Rock';