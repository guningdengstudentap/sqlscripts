USE ModernWaysjoins;
SELECT Games.Titel, Platformen.Naam
FROM Games LEFT JOIN
(Platformen INNER JOIN Releases ON Releases.Platformen_Id = Platformen.Id) -- eerst uitgewerkt
ON Releases.Games_Id = Games.Id;

/*
FROM Releases
	 INNER JOIN Games ON Releases.Games_Id = Games.Id
     INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id;
     -- RIGHT JOIN Games ON Releases.games_Id = Games.Id;
     */