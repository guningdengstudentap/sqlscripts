USE ModernwaysGSO;
SELECT Artiest, SUM(AantalBeluisteringen) AS 'Specifiek aantal beluisteringen'
FROM Liedjes
GROUP BY Artiest
HAVING SUM(AantalBeluisteringen) IN (17, 50, 100);