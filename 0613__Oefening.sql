USE ModernWays;
DROP VIEW IF EXISTS AuteursBoeken;
CREATE VIEW AuteursBoeken
AS
SELECT Personen.Id AS '#', CONCAT(Personen.Voornaam, ' ', Personen.Familienaam) AS 'Auteurs', Boeken.Titel, Publicaties.Boeken_Id
FROM Personen INNER JOIN
(Boeken INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_Id)
ON Publicaties.Personen_Id = Personen.Id;
/*
FROM (Publicaties INNER JOIN Personen ON Personen.Id = Publicaties.Personen_Id)
INNER JOIN Boeken ON Boeken.Id = Publicaties.Boeken_Id;
*/