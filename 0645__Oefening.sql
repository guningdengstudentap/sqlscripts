USE Aptunes;
DROP PROCEDURE IF EXISTS MockAlbumreleaseWithSuccess;

DELIMITER $$
USE Aptunes$$
CREATE DEFINER=`root`@`localhost` PROCEDURE MockAlbumreleaseWithSuccess (OUT success BOOL)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

SELECT COUNT(*) 
INTO numberOfAlbums
FROM Albums;

SELECT COUNT(*)
INTO numberOfBands
FROM Bands;

SET randomAlbumId=FLOOR(RAND()*numberOfAlbums)+1;

SET randomBandId=FLOOR(RAND()*numberOfBands)+1;

IF(randomBandId, randomAlbumId)
    NOT IN(SELECT DISTINCT * FROM AlbumReleases)
THEN
	INSERT INTO ALbumReleases (Bands_Id, Albums_Id)
    VALUES(randomBandId, randomAlbumId);
	
    SET success = 1;
ELSE 
	SET success = 0;
END IF;

END$$

DELIMITER ;






