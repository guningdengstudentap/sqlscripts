USE ModernWays;
CREATE TABLE Boeken(
Voornaam VARCHAR(50) CHAR SET utf8mb4,
FamilieNaam VARCHAR(80) CHAR SET utf8mb4,
Titel VARCHAR(255) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
Verschijningsjaar VARCHAR(4),
Uitgevrij VARCHAR(80) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(1000)
);