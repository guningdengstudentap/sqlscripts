USE ModernwaysGSO;
SELECT Artiest, SUM(Aantalbeluisteringen) 
AS 'Totaal aantal beluisteringen'
FROM Liedjes
WHERE LENGTH(Artiest) >= 10
GROUP BY Aantalbeluisteringen >= 100;