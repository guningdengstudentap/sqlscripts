USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN Titel VARCHAR(100), IN bands_Id INT)
BEGIN
START TRANSACTION;
INSERT INTO Albums(Titel)
VALUES (Titel);
INSERT INTO Albumreleases (Bands_Id, Albums_Id)
VALUES(bands_Id,last_insert_Id());
COMMIT;

END$$
DELIMITER ;

-- CALL CreateAndReleaseAlbum('innovate scalable content',2);
