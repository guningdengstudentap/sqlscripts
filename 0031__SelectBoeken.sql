USE modernwaysdmlmedium;
SELECT Voornaam, Familienaam, Titel 
FROM Boeken
ORDER BY Familienaam ASC, Voornaam, Titel;
SELECT Voornaam, Familienaam, Titel 
FROM Boeken
ORDER BY Familienaam DESC, Voornaam, Titel;