USE ModernWays;
SELECT Id
FROM Studenten INNER JOIN Evaluaties
ON Studenten.Id = Evaluaties.Studenten_Id
GROUP BY Studenten.Id
HAVING AVG(Cijfer) > (SELECT AVG(Cijfer) FROM Evaluaties);