USE Aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleases;

DELIMITER $$
USE Aptunes$$
CREATE PROCEDURE MockAlbumreleases(IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE success BOOL;

REPEAT	
    CALL MockAlbumreleaseWithSuccess(success);
		IF success
		THEN
			SET counter = counter+1;
		END IF;
	UNTIL counter >= extraReleases
END REPEAT;

END$$

DELIMITER ;
 -- MockAlbumReleases


     