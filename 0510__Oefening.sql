USE ModernWaysDb;
INSERT INTO Liedjes(Titel, Artiest, Jaar, Album)
VALUES
('Stairway to Heaven', 'Led Zeppelin', '1971', 'Led Zeppelin IV'),
('Good Enough', 'Tuttle','2017', 'Rise'),
('Outrage for the Execution of Willie McGee', 'Goodnight, Texas', '2018', 'Conductor'),
('They Lie', 'Layla Zoe', '2013', 'The Lily'),
('It Ain''t You', 'Danielle Nicole', '2015', 'Wolf Den'),
('Unchained', 'Van Halen', '1981', 'Fair Warning');