USE ModernWays;
SELECT Voornaam, Familienaam, Loon
FROM Directieleden
WHERE Loon < ANY (SELECT MAX(Loon) FROM Personeelsleden)
ORDER BY Loon;