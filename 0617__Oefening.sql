USE ModernWays;
DROP VIEW IF EXISTS AuteursBoekenRatings;
CREATE VIEW AuteursBoekenRatings
AS
SELECT AuteursBoeken.Auteurs AS 'Auteur', 
       AuteursBoeken.Titel AS 'Titel', 
       GemiddeldeRatings.Rating AS 'Rating'
FROM AuteursBoeken
INNER JOIN GemiddeldeRatings ON (AuteursBoeken.Boeken_Id = GemiddeldeRatings.Boeken_Id);


