USE ModernWays;
DROP VIEW IF EXISTS GemiddeldeRatings;
CREATE VIEW GemiddeldeRatings
AS
SELECT Reviews.Boeken_Id AS 'Boeken_Id', AVG(Reviews.Rating) AS 'Rating'
FROM Reviews
GROUP BY Reviews.Boeken_Id;


/*AS
SELECT Reviews.Boeken_Id AS 'Boeken_Id',
      AVG(Reviews.Rating) AS 'Rating'
FROM Reviews
GROUP BY Reviews.Boeken_Id;
*/