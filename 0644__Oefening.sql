USE Aptunes; 
DROP PROCEDURE IF EXISTS MockAlbumRelease;

DELIMITER $$
USE Aptunes$$
CREATE DEFINER=`root`@`localhost` PROCEDURE MockAlbumRelease()
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

SELECT COUNT(*) 
FROM Albums
INTO numberOfAlbums;

SELECT COUNT(*)
FROM Bands
INTO numberOfBands;

SELECT FLOOR(RAND()*numberOfAlbums)+1
INTO randomAlbumId;

SELECT FLOOR(RAND()*numberOfBands)+1
INTO randomBandId;

IF(SELECT Bands_Id 
	FROM AlbumReleases
    WHERE Bands_Id=randomBandId AND Albums_Id=randomAlbumId)
    IS NULL
THEN
	INSERT INTO ALbumReleases (Bands_Id, Albums_Id)
    VALUES (randomBandId, randomAlbumId);
END IF;
END$$

DELIMITER ;

-- CALL MockAlbumRelease()





