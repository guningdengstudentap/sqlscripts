USE Aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleasesLoop;

DELIMITER $$
USE Aptunes$$
CREATE DEFINER=`root`@`localhost` 
PROCEDURE MockAlbumReleasesLoop (IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE success BOOL;

loop_MockAlbumReleases: LOOP
	CALL MockAlbumReleaseWithSuccess(success);
    
    IF success 
    THEN
		SET counter = counter+1;
	END IF;
    
    IF counter >= extraReleases
    THEN
		LEAVE loop_MockAlbumReleases;
	END IF;
END LOOP;
    
END$$

DELIMITER ;







