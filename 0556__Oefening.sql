USE ModernWaysJOINs;
SELECT huisdieren.Naam AS 'Naam huisdier', baasjes.Naam AS 'Naam baasje'
FROM huisdieren INNER JOIN baasjes
ON huisdieren.Id = Huisdieren_Id;