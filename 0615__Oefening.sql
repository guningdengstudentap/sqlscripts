USE ModernWays;
DROP VIEW IF EXISTS AuteursBoeken;
CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(Personen.Voornaam, ' ', Personen.Familienaam) AS 'Auteur',
Boeken.Titel AS 'Titel',
Boeken.Id AS 'Boeken_Id'
FROM Publicaties INNER JOIN Personen ON (Personen.Id = Publicaties.Personen_Id)
				 INNER JOIN Boeken ON (Boeken.Id = Publicaties.Boeken_Id);