USE ModernWaysJOINs;
ALTER TABLE Baasjes
ADD COLUMN Huisdieren_Id INT,
ADD CONSTRAINT fk_Baasjes_Huisdieren FOREIGN KEY (Huisdieren_Id)
REFERENCES huisdieren(Id);