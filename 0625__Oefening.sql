USE Aptunes;
SELECT Voornaam, Familienaam, COUNT(Lidmaatschappen.Muziekanten.Id)
FROM Muzikanten INNER JOIN Lidmaatschappen
ON Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
GROUP BY Familienaam, Voornaam
ORDER BY Voornaam, Familienaam;