USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT Aantal TINYINT)
BEGIN
SELECT count(*)
INTO Aantal
FROM Genres;

END$$
DELIMITER ;

-- CALL NumberOfGenres(@Aantal); -- mogelijk @Aantal=anyName
-- SELECT @Aantal; -- mogelijk @Aantal=anyName